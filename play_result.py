"""Results visualization."""

from time import sleep

import gym
import numpy as np

MAX_STEPS = 2000


env = gym.wrappers.TimeLimit(gym.make('CartPole-v1').unwrapped, max_episode_steps=MAX_STEPS)
state = env.reset()

mx = np.load('hill_climbing.npy')  # load best weights  'random_search.npy'  'hill_climbing.npy'  'strategy_gradient.npy'
# mx = torch.rand((env.observation_space.shape[0], env.action_space.n))  # load random weights

is_done = False
while not is_done:
    env.render()
    action = np.matmul(state[None, :], mx).argmax().item()
    state, reward, is_done, info = env.step(action)
    sleep(0.01)

env.close()
