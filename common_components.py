"""Some common objects to be used in another modules."""

import numpy as np


def estimate_mx(env,
                mx: np.array,
                n: int = 1,
                break_episode_if_not_best: bool = False,
                best_possible_reward: int = None) -> float:
    """Runs episode n times with weights mx. Returns mean reward.
    If continue_if_not_best set as True you should set set best_possible_reward (as MAX_STEPS).
    """

    if break_episode_if_not_best and not best_possible_reward:
        raise Exception("If you use 'continue_if_not_best' you should set best_possible_reward as MAX_STEPS")

    total_reward = 0
    for _ in range(n):
        state = env.reset()
        is_done = False

        # run episode
        while not is_done:
            # action = torch.mm(torch.from_numpy(state)[None, :], mx).argmax().item()
            action = np.matmul(state[None, :], mx).argmax().item()
            state, reward, is_done, info = env.step(action)
            total_reward += reward

        # to break evaluation if we already got not best result
        if break_episode_if_not_best and total_reward < best_possible_reward:
            break

    return total_reward / n
