"""Hill climbing (by adding noise) solution."""

import gym
import numpy as np

from common_components import estimate_mx

# parameters
EPOCHS = 1000
RUN_N_EPISODES = 1000  # number of episodes to be run for mean reward estimation
MAX_STEPS = 200  # steps of CartPole to play per one training run
BREAK_EPISODE_IF_NOT_BEST = True  # should we break episode estimation if we get not best result
SCALE = 0.2  # scale training speed (essentially - learning rate)
# SCALE_CHANGE = 1.2  # SCALE change per step


env = gym.wrappers.TimeLimit(gym.make('CartPole-v1').unwrapped, max_episode_steps=MAX_STEPS)

best_mx = None
best_total_reward = 0
mx = np.random.rand(env.observation_space.shape[0], env.action_space.n)

for i in range(EPOCHS):
    env.reset()
    mx_new = mx + SCALE * np.random.rand(env.observation_space.shape[0], env.action_space.n)

    total_reward = estimate_mx(env,
                               mx_new,
                               RUN_N_EPISODES)
    if total_reward >= best_total_reward:
        mx = mx_new
        best_mx = mx.copy()
        best_total_reward = total_reward
        # SCALE = max(SCALE / SCALE_CHANGE, 5e-2)
    else:
        # SCALE = min(SCALE * (1 + 0.2 * (SCALE_CHANGE - 1)), 1)
        pass

    print(f"\r{i:>5}    current mean reward = {total_reward:<7}    "
          f"best mean reward = {best_total_reward:<7}    SCALE = {SCALE}")
    print(' ... \U000023F3 ... WAIT ... \U000023F3 ... ', end='')
    if total_reward == MAX_STEPS:
        break

env.close()

print('\rbest -> ', best_total_reward)

np.save('hill_climbing.npy', best_mx)
