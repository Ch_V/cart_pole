"""QNet returns values of actions."""

import gym
import torch
import torch.nn as nn
import torch.nn.functional as F
from torchinfo import summary

from common_components import env

LAYER_LEN = 6

n_state_params = env.observation_space.shape[0]
n_actions = env.action_space.n


class QNet(nn.Module):
    def __init__(self):
        super().__init__()
        self.layers = nn.Sequential(
            nn.Linear(n_state_params, LAYER_LEN),
            nn.ReLU(),
            nn.Linear(LAYER_LEN, n_actions)
        )

    def forward(self, x):
        x = self.layers(x)
        return x


class QNetDueling(nn.Module):
    def __init__(self):
        super().__init__()
        self.fc1 = nn.Linear(n_state_params, LAYER_LEN)
        self.fc_v = nn.Linear(LAYER_LEN, 1)
        self.fc_a = nn.Linear(LAYER_LEN, n_actions)
        
    def forward(self, x):
        x = F.relu(self.fc1(x))
        v = self.fc_v(x)
        a = self.fc_a(x)
        return v, a


if __name__ == '__main__':
    summary(QNet(), (n_state_params,))
    print('\n' * 3)
    summary(QNetDueling(), (n_state_params,))
