"""Random search solution."""

import gym
import numpy as np

from common_components import estimate_mx

# parameters
MAX_RUNS = 100
RUN_N_EPISODES = 1000  # number of episodes to be run for mean reward estimation
MAX_STEPS = 200  # steps of CartPole to play per one training run
BREAK_EPISODE_IF_NOT_BEST = True  # should we break episode estimation if we get not best result
env = gym.make('CartPole-v1')  # v0 for 200 steps; v1 for 500 steps


env = gym.wrappers.TimeLimit(gym.make('CartPole-v1').unwrapped, max_episode_steps=MAX_STEPS)

best_mx = None
best_total_reward = 0

for i in range(MAX_RUNS):
    env.reset()
    mx = np.random.rand(env.observation_space.shape[0], env.action_space.n)  # random weights

    total_reward = estimate_mx(env,
                               mx,
                               RUN_N_EPISODES,
                               break_episode_if_not_best=BREAK_EPISODE_IF_NOT_BEST,
                               best_possible_reward=MAX_STEPS)
    if total_reward > best_total_reward:
        best_mx = mx.copy()
        best_total_reward = total_reward

    print(f"\r{i:>5}    current mean reward = {total_reward:<7}    best mean reward = {best_total_reward}")
    print(' ... \U000023F3 ... WAIT ... \U000023F3 ... ', end='')

    if total_reward >= MAX_STEPS:
        break

env.close()

print('\rbest -> ', best_total_reward)

np.save('random_search.npy', best_mx)
